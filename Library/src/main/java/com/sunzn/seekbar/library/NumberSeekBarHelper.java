package com.sunzn.seekbar.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class NumberSeekBarHelper {

    private static final int DEFAULT_TEXT_COLOR = 0XFFFF0000;

    // 控件上下文
    private Context context;

    // 控件索引
    private NumberSeekBar seekBar;

    // 文字画笔
    private TextPaint paint;

    // 文字大小
    private final int textSize;

    // 文字颜色
    private final int textColor;

    // 文字是否隐藏
    private final boolean textFade;

    // 文字隐藏延时
    private final float textTime;

    // 文字底部间距
    private final int textSpace;

    // 文字隐藏标志
    private boolean hideMark;

    // 文字隐藏动作
    private Runnable runnable;

    // 文字边距测量
    private final Rect textRect = new Rect();

    // 进度监听器
    private NumberSeekBar.OnSeekBarChangeListener listener;


    public NumberSeekBarHelper(@NonNull NumberSeekBar seekBar, @NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        storeParams(seekBar, context);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.NumberSeekBar, defStyleAttr, 0);
        textSize = attributes.getDimensionPixelSize(R.styleable.NumberSeekBar_nsb_text_size, sp2px(12));
        textColor = attributes.getColor(R.styleable.NumberSeekBar_nsb_text_color, DEFAULT_TEXT_COLOR);
        textFade = attributes.getBoolean(R.styleable.NumberSeekBar_nsb_text_fade, true);
        textTime = attributes.getFloat(R.styleable.NumberSeekBar_nsb_text_time, 0);
        textSpace = attributes.getDimensionPixelSize(R.styleable.NumberSeekBar_nsb_text_space, dp2px(0));
        attributes.recycle();
        init();
    }

    private void storeParams(@NonNull NumberSeekBar seekBar, @NonNull Context context) {
        this.seekBar = seekBar;
        this.context = context;
    }

    private void init() {
        paint = new TextPaint();
        paint.setAntiAlias(true);
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        initPadding(seekBar);
        initMonitor(seekBar);
    }

    private void initPadding(@NonNull NumberSeekBar seekBar) {
        int thumbWidth = seekBar.getThumb().getMinimumWidth() / 2;
        seekBar.setPadding(getPaddingStart() + thumbWidth, getPaddingTop() + textSize + textSpace, getPaddingEnd() + thumbWidth, getPaddingBottom());
    }

    private void initMonitor(@NonNull NumberSeekBar seekBar) {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (listener != null) {
                    listener.onProgressChanged(seekBar, progress, fromUser);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                hideMark = false;
                seekBar.removeCallbacks(runnable);
                if (listener != null) {
                    listener.onStartTrackingTouch(seekBar);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (textFade && textTime > 0) {
                    runnable = initRunnable();
                    seekBar.postDelayed(runnable, (long) textTime);
                }
                if (listener != null) {
                    listener.onStopTrackingTouch(seekBar);
                }
            }
        });
    }

    public void onDraw(Canvas canvas) {
        String progress = hideMark ? "" : String.valueOf(seekBar.getProgress());
        paint.getTextBounds(progress, 0, progress.length(), textRect);
        float ratio = (float) seekBar.getProgress() / seekBar.getMax();
        float numberX = getPaddingStart() + (getWidth() - getPaddingStart() - getPaddingEnd()) * ratio - textRect.width() / 2F;
        float numberY = getHeight() - getPaddingBottom() - getThumb().getMinimumHeight() - textSpace;
        canvas.drawText(progress, numberX, numberY, paint);
    }

    private Runnable initRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                hideText(true);
            }
        };
    }

    public void hideText(boolean mark) {
        hideMark = mark;
        seekBar.invalidate();
    }

    private Drawable getThumb() {
        return seekBar.getThumb();
    }

    private int getWidth() {
        return seekBar.getWidth();
    }

    private int getHeight() {
        return seekBar.getHeight();
    }

    private int getPaddingStart() {
        return seekBar.getPaddingStart();
    }

    private int getPaddingTop() {
        return seekBar.getPaddingTop();
    }

    private int getPaddingEnd() {
        return seekBar.getPaddingEnd();
    }

    private int getPaddingBottom() {
        return seekBar.getPaddingBottom();
    }

    public void setProgress() {
        hideMark = false;
        seekBar.removeCallbacks(runnable);
        runnable = initRunnable();
        seekBar.postDelayed(runnable, (long) textTime);
    }

    public void setOnSeekBarChangeListener(NumberSeekBar.OnSeekBarChangeListener listener) {
        this.listener = listener;
    }

    private int sp2px(float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public int dp2px(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

}
