package com.sunzn.seekbar.library;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;

public class NumberSeekBar extends AppCompatSeekBar {

    public interface OnSeekBarChangeListener {

        void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser);

        void onStartTrackingTouch(SeekBar seekBar);

        void onStopTrackingTouch(SeekBar seekBar);
    }

    private final NumberSeekBarHelper helper;

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener listener) {
        helper.setOnSeekBarChangeListener(listener);
    }

    public NumberSeekBar(@NonNull Context context) {
        this(context, null);
    }

    public NumberSeekBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberSeekBar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        helper = new NumberSeekBarHelper(this, context, attrs, defStyleAttr);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        helper.onDraw(canvas);
    }

    @Override
    public synchronized void setProgress(int progress) {
        super.setProgress(progress);
        if (helper != null) {
            helper.setProgress();
        }
    }

}
